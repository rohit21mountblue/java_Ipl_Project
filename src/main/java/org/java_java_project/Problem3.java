package org.java_java_project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//    3. For the year 2016 get the extra runs conceded per team.
public class Problem3 {

    public static Map extraruns(List<Match_Data> matches, List<Delivery_Data> deliveries){
        Map<String, Integer> extraRunsByTeam = new HashMap<>();
        List<String> matchesID=new ArrayList<String>();

        for (Match_Data match : matches) {
            if (match.getSeason().equals("2016")){
                matchesID.add(match.getId());
            }
        }

        for (Delivery_Data delivery : deliveries) {
            if (matchesID.contains(delivery.getMatch_id())) {
                if (extraRunsByTeam.containsKey(delivery.getBatting_team())) {
                    int extraRun = extraRunsByTeam.get(delivery.getBatting_team());
                    extraRunsByTeam.replace(delivery.getBatting_team(), extraRun + Integer.parseInt(delivery.getExtra_runs()));
                } else {
                    extraRunsByTeam.put(delivery.getBatting_team(), Integer.parseInt(delivery.getExtra_runs()));
                }
            }
        }
        return extraRunsByTeam;
    }
}
